# HTML AVANZADO [Formularios]

## 1. Atributos principales en HTML5

### Etiqueta `<form>`

| Etiquetas   | Descripción                                                                                                                                                                                                             |
| ----------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **action**  | Especifica la URL del archivo del servidor al que se enviarán los datos del formulario para su procesamiento.                                                                                                           |
| **method**  | Indica el método de envío de los datos del formulario. Puede ser "GET" o "POST". "GET" coloca los datos en la URL, mientras que "POST" los envía en el cuerpo de la solicitud HTTP.                                     |
| **target**  | Especifica el destino donde se abrirá la respuesta del servidor después de enviar el formulario. Puede ser "\_blank", "\_self", "\_parent", o "\_top", entre otros.                                                     |
| **enctype** | Indica cómo deben codificarse los datos antes de enviarlos al servidor. Los valores comunes son "application/x-www-form-urlencoded" (predeterminado), "multipart/form-data" (usado para subir archivos) y "text/plain". |

### Etiqueta `<input>`

| Etiquetas       | Descripción                                                                                                                           |
| --------------- | ------------------------------------------------------------------------------------------------------------------------------------- |
| **type**        | Especifica el tipo de entrada que el elemento `<input>` representa, como texto, contraseña, número, etc.                              |
| **name**        | Proporciona un nombre para el elemento `<input>`, que se utiliza para identificar el campo cuando se envían los datos del formulario. |
| **value**       | Establece el valor inicial del elemento `<input>`. Para los campos de texto, este es el valor que se mostrará inicialmente.           |
| **required**    | Si está presente, indica que el campo es obligatorio y no se puede enviar el formulario si está vacío.                                |
| **placeholder** | Proporciona un texto de ejemplo o indicación que se muestra en el campo antes de que se ingrese cualquier valor.                      |

### Etiqueta `<label>`

| Etiquetas | Descripción                                                                                                                                                                                                              |
| --------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| **for**   | Este atributo establece una asociación entre el elemento `<label>` y un elemento de formulario específico. El valor debe ser el mismo que el atributo "id" del elemento de formulario al que deseas asociar la etiqueta. |

### Etiqueta `<button>`

| Etiquetas    | Descripción                                                                                                                                  |
| ------------ | -------------------------------------------------------------------------------------------------------------------------------------------- |
| **type**     | Define el tipo de botón. Los valores comunes son "submit" (enviar formulario), "reset" (restablecer formulario) y "button" (botón genérico). |
| **name**     | Proporciona un nombre para el botón, que se utiliza para identificarlo cuando se envían los datos del formulario.                            |
| **value**    | Establece el valor que se enviará al servidor cuando el botón sea presionado. Esto se usa junto con el atributo "name" para enviar datos.    |
| **disabled** | Si está presente, deshabilita el botón, lo que significa que no se puede hacer clic ni interactuar con él.                                   |

## 2. Código

```html
<form action="recibir.html" method="GET">
    <label for="text">Texto: </label>  
  <input
    type="text"
    name="text"
    id="text"
    placeholder="Ingrese un texto por favor"
  />
    <button type="submit">Enviar</button>
</form>
```

## 3. Imagen del formulario

![Descripción de la imagen](formulario.PNG)
